const mongoose = require('mongoose')

const Game = mongoose.model('Game', {
    winner: {
        type: String,
        required: true,
        trim: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    
})

module.exports = Game