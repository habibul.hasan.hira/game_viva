const express = require('express');
require('./db/mongoose');
const gameRouter = require('./routers/game');
const cors = require('cors');


const app = express()

const port = process.env.PORT || 5000
app.use(cors({ origin: true }));
app.use(express.json())
app.use(userRouter)
app.use(taskRouter)

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})

const Task = require('./models/Game')
 