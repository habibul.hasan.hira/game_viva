const express = require('express')
const Game = require('../models/Game')
const router = new express.Router()

/** 
 * to save a games winner information into mongo
 * @param req
 * @param res
*/
router.post('/games', async (req, res) => {
    const game = new Game({
        winner: req.body.winner,
        completed: req.body.completed
    })

    try {
        await game.save()
        res.status(201).send(game);
      
        
    } catch (e) {
        res.status(400).send(e)
    }
})



/** 
 * to get all games results
 * @param req
 * @param res
*/
router.get('/gamesRestlt', async (req, res) => {
    const _id = req.params.id;

    try {
        const gamesRestlt = await Game.find();
        res.send(gamesRestlt);
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router