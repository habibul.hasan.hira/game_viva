# Project Title

Tic Tac Toe Built in JS [Mongo , React, Redux, Node js]

*  NODE
*  EXRESS
*  REACT
*  MONGODB
*  Mongoose
*  ES6

## Getting Started

This document will give specific information about how to run this project in a given environemnt. This project has two part 
one is backend another is client application in react. Backend and front end. 
### Prerequisites

Before installing the software please do check the below requirements 

```
node LTS
```

```
mongodb
```

```
React LTS
```


### Installing



Installing npm modules for server, go to app folder and open terminal on root folder and run 

```
npm install --ver 
```

Installing npm modules for client , go to client folder and open terminal on root folder and run

```
npm install --ver
```

To run the entire application concurrently used, to run concurrently, go to app folder and run 

## npm run dev

API end points 

### Break down into end to end tests


To save game winner  , send data with body by setting POST as request type
```
SET TYPE: POST [in postman]
http://localhost:5000/game
```

```
SET TYPE: GET  [in postman] to get all games winning information
http://localhost:5000/games
```

## Built With

* [NODE](Server side ) 
* [Express](web framework) 
* [Recct](front end tool) 
* [React_Redux](front end tool) 
* [mongodb](database)
* [mongoose](ORM)


## Versioning

Used git as a versioning tool.

## Authors

**Habibul Hasan Hira** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Stack overflow
* React website
* Redux website 
* Node js docs 
* Mongo docs
