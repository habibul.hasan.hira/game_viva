import { SET_NEXT_PLAYER } from '../helpers/actionTypes';
export const  setNextPlayer = (data) =>  {
    return {
        type: SET_NEXT_PLAYER,
        payload:{
            nextPlayer:data
        }
      }
}