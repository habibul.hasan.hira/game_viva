import React from 'react';
import '../App.css';
function Box(props) {
    return (
      <div className="flex-item" onClick={props.onClick}>
       <p  className="player">
        {props.value}
       </p>
      </div>
    );
}
export default Box;