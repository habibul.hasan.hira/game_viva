import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import GameBoard from './Game';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux'
import {store,persistor} from './store';
import { PersistGate } from 'redux-persist/integration/react'

ReactDOM.render(
  <Provider store={store}>
     <PersistGate loading={null} persistor={persistor}>
         <GameBoard />
      </PersistGate>
  </Provider>,
  document.getElementById('root')
 );
serviceWorker.unregister();
