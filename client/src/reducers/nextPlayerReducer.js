import { SET_NEXT_PLAYER } from '../helpers/actionTypes';
export default (state = {nextPlayer:'X'}, action) => {
    switch (action.type) {
     case SET_NEXT_PLAYER:
      return {...state, nextPlayer:action.payload.nextPlayer}
     default:
      return state
    }
}