import { combineReducers } from 'redux';
import nextPlayerReducer from './nextPlayerReducer';
import playersPositionReducer from './playersPositionReducer';
export default combineReducers({
 nextPlayer: nextPlayerReducer,
 grid: playersPositionReducer
});