import { SET_PLAYER_POSITION } from '../helpers/actionTypes';
export default (state = {grid:Array(9).fill(null)}, action) => {
    switch (action.type) {
     case SET_PLAYER_POSITION:
      return {...state, grid:action.payload.grid}
     default:
      return state
    }
}