import React from 'react';
import { connect } from 'react-redux';
import Box from './components/Box';
import getWinnerPlayer from './helpers/GameEngine';
import {setNextPlayer} from './actions/setNextPlayerAction';
import {setNextPlayerPosition} from './actions/setPlayerPositonAction';
import {persistor} from './store';
import './App.css';

class GameBoard extends React.Component {
  constructor(props) {
    super(props);
  }
  handleClick(i) {
    const grid = this.props.grid.slice();
    if (getWinnerPlayer(grid) || grid[i]) {
      this.props.setNextPlayerPosition(Array(9).fill(null));
      return;
    }
    this.props.setNextPlayer(this.props.nextPlayer === 'X'? 'O' : 'X');
    grid[i] = this.props.nextPlayer ==='X'? 'X' : 'O';
    this.props.setNextPlayerPosition(grid);
  }
  renderBox(i) {
    return (
      <Box
        key = {Math.random(1,200)}
        value={this.props.grid[i]}
        onClick={() => this.handleClick(i)}
      />
    );
  }

  render() {
    let boxsize = [0,1,2,3,4,5,6,7,8];
    let gameBoxes = boxsize.map((value)=>{
      return  this.renderBox(value);
    });
    const winner = getWinnerPlayer(this.props.grid);
    return ( 
        <div className="container">
            <div className="status">PLAY TIC TAC TOE</div>
            <div className="board-container">
              {gameBoxes}
            </div>
            <div className="status">
              { winner?`Winner is: ${winner} , Click to start again`: `Please make yor move player:  ${this.props.nextPlayer}`}
             
            </div>
        </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setNextPlayer: msg => {
    dispatch(setNextPlayer(msg));
  },
  setNextPlayerPosition: msg => {
    dispatch(setNextPlayerPosition(msg));
  }
 });
 
 const mapStateToProps = (state) => {
   console.log(state);
   return {...state.nextPlayer,...state.grid}
  };
 export default connect(mapStateToProps, mapDispatchToProps)(GameBoard);
